# install nodejs
```bash
wget https://nodejs.org/dist/v9.9.0/node-v9.9.0-linux-x64.tar.gz
```


```bash
sudo tar -C /usr/local --strip-components 1 -xzf node-v9.9.0-linux-x64.tar.gz node-v9.9.0-linux-x64/
sudo npm install --global yarn
```

# install nodejs
### install dependencies
``bash
yarn install
``

### local development
```bash
yarn start
```

### local build
```bash
yarn build
```


### build & patch

```bash
yarn patch
```