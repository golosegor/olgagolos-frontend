const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');

const proxy = {
    target: 'http://localhost:8080',
    changeOrigin: true,
    secure: false,
    logLevel: 'debug',
    router: () => {
        return 'https://olgagolos.com/';
    },
};

const devServer = {
    contentBase: path.join(__dirname, 'dist'),
    stats: {
        colors: true,
    },
    proxy: {
        '/olgagolos/api/': proxy,
        '/olgagolos/auth/': proxy
    },
    port: 8080,
    hot: true,
    progress: true
};

const getPlugins = (indexPath) => [
    new HtmlWebpackPlugin({
        template: indexPath,
    }),
    new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: JSON.stringify(process.env.NODE_ENV),
        },
    }),
    new MiniCssExtractPlugin({
        filename: '[name].[contenthash].css',
        disable: process.env.NODE_ENV !== 'production',
    }),
    new CopyWebpackPlugin([
        {from: './src/favicon.ico', to: 'favicon.ico'}
    ]),
    new webpack.HotModuleReplacementPlugin()
];

const rules = [
    {
        test: /\.(tsx|ts)$/,
        exclude: /(node_modules)/,
        loader: 'babel-loader',
    },
    {
        test: /\.css$/,
        exclude: /(node_modules)/,
        use: [
            MiniCssExtractPlugin.loader,
            {
                loader: 'css-loader',
                options: {
                    modules: true,
                    localIdentName: '[name]---[local]---[hash:base64:5]',
                    url: false,
                },
            }
        ]
    }
];
module.exports = [
    {
        entry: './src/index.tsx',
        output: {
            filename: 'bundle.[hash].js',
            path: path.resolve(__dirname, 'dist'),
            chunkFilename: "[name].chunk.js"
        },
        resolve: {
            extensions: ['.ts', '.tsx', '.js'],
            alias: {
                react: "preact-compat",
                "react-dom": "preact-compat"
            }
        },
        devtool: 'source-map',
        module: {
            rules: rules
        },
        devServer: devServer,
        plugins: getPlugins('./src/index.html')
    },
    {
        entry: './src/admin/index.tsx',
        output: {
            filename: 'bundle.[hash].js',
            path: path.resolve(__dirname, 'dist/admin'),
            chunkFilename: "[name].chunk.js"
        },
        resolve: {
            extensions: ['.ts', '.tsx', '.js'],
            alias: {
                react: "preact-compat",
                "react-dom": "preact-compat"
            }
        },
        devtool: 'source-map',
        module: {
            rules: rules
        },
        devServer: devServer,
        plugins: getPlugins('./src/admin/index.html')
    }
];