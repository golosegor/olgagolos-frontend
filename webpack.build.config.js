const merge = require('webpack-merge');
const common = require('./webpack.config');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
module.exports = [
  merge(common[0], {
    devtool: 'source-map',
    plugins: [
      new CleanWebpackPlugin(['dist']),
      new OptimizeCssAssetsPlugin()
    ],
  }),
  merge(common[1], {
    devtool: 'source-map',
    plugins: [
      new OptimizeCssAssetsPlugin()
    ],
  })
];
