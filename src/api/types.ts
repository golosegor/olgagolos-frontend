export type Pagination = {
    skip: number;
    limit: number;
    direction?: 'ASC' | 'DESC';
    property?: string;
}

export type Exhibition = {
    address: string;
    features: string;
    exhibitionType: string;
    id: number;
    name: string;
    timeAndPlace: string;
};

export type Exhibitions = {
    content: Exhibition[];
    first: boolean;
    last: boolean;
    number: number;
    numberOfElements: number;
    size: number;
    sort: {};
    totalElements: number;
    totalPages: number;
};

export interface ISubscribers {
    content: ISubscriber[],
    numberOfElements: number,
    first: boolean,
    size: number,
    number: number
}

export interface ISubscriber {
    id: number,
    confirmed: boolean,
    name: string,
    surname: string,
    email: string,
    createdAt: number
}