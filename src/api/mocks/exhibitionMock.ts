import {Exhibitions} from "../types";

export default (): Exhibitions => ({
    content: [{
        address: 'Blablabla Street 1',
        features: 'features',
        exhibitionType: 'Current Exhibition',
        id: 1,
        name: 'SuperDuper hello world',
        timeAndPlace: 'sometime somewhere'
    }, {
        address: 'Blibli Street 10',
        exhibitionType: 'Upcoming Exhibition',
        features: 'features',
        id: 2,
        name: 'SuperDuper hello world 2',
        timeAndPlace: 'sometime somewhere wow'
    }],
    last: true,
    totalPages: 1,
    totalElements: 0,
    sort: null,
    numberOfElements: 0,
    first: true,
    size: 0,
    number: 0
});