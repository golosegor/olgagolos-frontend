import {getUrl} from "./getUrl";
import {fetchJson, fetchDefault} from "./fetch";

export const getMe = (): Promise<{ login: string }> => fetchJson({
    uri: getUrl('api/admin/me'),
    method: 'get'
});

export const auth = (params: URLSearchParams): Promise<{ login: string }> => fetchDefault({
    uri: getUrl('auth/password'),
    method: 'post',
    params
});