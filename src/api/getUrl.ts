export function getUrl(suffix: string): string {
    const {protocol, host} = window.location;
    return `${protocol}//${host}/olgagolos/${suffix}`;
}

export const isMock = location.search.includes("debug");
