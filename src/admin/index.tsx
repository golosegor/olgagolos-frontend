import * as React from 'react';
import {render} from 'react-dom';
import '../index.css';

let root;

function init() {
    // HMR requires that this be a require()
    let AdminApp = require<any>('./AdminApp/AdminApp').AdminApp;
    // render the app and save the new root element:
    root = render(<AdminApp />, document.body, root);
}


// initial render!
init();

// If this is webpack-dev-server, set up HMR :)
if (module.hot) module.hot.accept("./AdminApp/AdminApp", init);