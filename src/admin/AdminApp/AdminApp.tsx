import * as React from 'react';
import {App} from "../../App/App";
import {getMe} from "../../api/me";
import {Auth} from "../../Auth/Auth";

type Props = {};
type State = {
    login: string
};

export class AdminApp extends React.Component<Props, State> {
    componentWillMount() {
        this.getMe();
    }

    getMe = () => {
        getMe().then(admin => {
            this.setState({
                login: admin.login
            })
        })
    };

    render() {
        return this.state.login ?
            (<App isAdmin={this.state.login} />) :
            (<Auth onLogin={this.getMe} />);
    }
}
