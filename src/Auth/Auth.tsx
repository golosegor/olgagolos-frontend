import * as React from 'react';
import css from './Auth.css';
import {Box} from "../Box/Box";
import {FormEvent} from "react";
import {auth} from "../api/me";

type Props = {
    onLogin: () => void;
};
type State = {
    login: string;
    password: string;
    wrong: boolean;
};

export class Auth extends React.Component<Props, State> {
    login = () => {
        const {login, password} = this.state;
        const searchParams = new URLSearchParams();
        searchParams.set('login', login);
        searchParams.set('password', password);
        auth(searchParams)
            .then(() => {
                this.props.onLogin();
            })
            .catch((e) => {
                console.log(e);
                this.setState({
                    wrong: true
                })
            });
    };

    onChangePassword = (e: FormEvent<HTMLInputElement>) => {
        this.setState({
            password: e.currentTarget.value
        });
    };

    onChangeLogin = (e: FormEvent<HTMLInputElement>) => {
        this.setState({
            login: e.currentTarget.value
        });
    };

    render() {
        return (
            <div className={css.container}>
                <Box>
                    {this.state.wrong ? (<div className={css.wrong}>Try again</div>) : null}
                    <div className={css.box}>
                        <div className={css.item}>
                            <div>Login:</div>
                            <div><input onChange={this.onChangeLogin} /></div>
                        </div>
                        <div className={css.item}>
                            <div>Password:</div>
                            <div><input type='password' onChange={this.onChangePassword} /></div>
                        </div>
                        <div className={css.item}>
                            <div className={css.button} onClick={this.login}>Ok</div>
                        </div>
                    </div>
                </Box>
            </div>
        );
    }
}
