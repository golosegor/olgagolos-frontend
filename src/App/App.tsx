import * as React from 'react';
import css from './App.css';
import {UploadField} from '@navjobs/upload'
import {Box} from "../Box/Box";
import {NewsItem} from "../NewsItem/NewsItem";
import {
    getExhibitions,
    addExhibition,
    saveExhibition,
    uploadInvitation,
    removeExhibition,
    swapPriorities
} from "../api/exhibition";
import {Exhibition} from "../api/types";
import {Contact} from "../Contact/Contact";
import {getUrl} from "../api/getUrl";
import {uploadCV, uploadImage, uploadPortfolio} from "../api/upload";

type Props = {
    isAdmin: string
};
type State = {
    exhibitions: Exhibition[];
    active: string;
};

export class App extends React.Component<Props, State> {
    state = {
        active: 'news',
        exhibitions: [] as Exhibition[]
    };
    portfolio = getUrl('api/portfolio');
    resume = getUrl('api/resume');
    bgImageGlobal = getUrl('api/background/image/GLOBAL');
    bgImageExhibition = getUrl('api/background/image/EXHIBITION');

    componentDidMount() {
        this.loadExhibitions();
    }

    loadExhibitions = () => {
        getExhibitions({
            skip: 0,
            limit: 10
        }).then(exhibitions => {
            this.setState({
                exhibitions: exhibitions.content
            });
        });
    };

    addNew = (files: FileList) => {
        addExhibition(files).then(newExhibition => {
            this.setState({
                exhibitions: [newExhibition, ...this.state.exhibitions]
            })
        });
    };

    onChangeExhibition = (id: number, type: string, content: string) => {
        this.setState({
            exhibitions: this.state.exhibitions.map(item => {
                if (item.id === id) {
                    item[type] = content;
                }
                return item;
            })
        })
    };

    onSaveExhibition = (id: number) => {
        saveExhibition(this.state.exhibitions.find(item => item.id === id))
            .then(() => console.log(`Exhibition ${id} saved`))
            .catch(() => console.error(`Exhibition ${id} not saved`));
    };

    onRemove = (id: number) => {
        removeExhibition(id)
            .then(() => {
                this.setState({
                    exhibitions: this.state.exhibitions.filter(item => item.id !== id)
                })
            }).then(() => console.log(`Exhibition ${id} removed`))
            .catch(() => console.error(`Exhibition ${id} not removed`));
    };

    move = (id: number, where: number) => {
        const exhibitions = this.state.exhibitions;
        const currentInd = exhibitions.findIndex(item => item.id === id);
        const whereId = exhibitions[currentInd + where].id;

        swapPriorities({
            swapExhibitionIdFrom: id,
            swapExhibitionIdTo: whereId
        })
            .then(this.loadExhibitions)
            .then(() => console.log(`Exhibition swap`))
            .catch(() => console.error(`Exhibition not swap`));
    };

    onMoveUp = (id: number) => {
        this.move(id, -1);
    };

    onMoveDown = (id: number) => {
        this.move(id, 1);
    };

    uploadImage = (files: FileList) => {
        uploadImage(files, this.state.active === 'news' ? 'EXHIBITION' : 'GLOBAL')
            .then(() => console.log(`Image ${files[0].name} loaded`))
            .catch(() => console.error(`Image ${files[0].name} not loaded`));
    };

    render() {
        const {isAdmin} = this.props;
        const {exhibitions, active} = this.state;
        return (
            <div className={css.container} style={{
                backgroundImage: `url(${
                    active === 'news' ?
                        this.bgImageExhibition :
                        this.bgImageGlobal})`
            }}>
                <header className={css.header}>
                    <div className={css.title} onClick={() => this.setState({
                        active: 'news'
                    })}>
                        <svg xmlns="http://www.w3.org/2000/svg" height="100%" viewBox="0 0 233 29">
                            <path
                                d="M115.4 4.8c-5.8 5.1-6.6 12.8-1.5 18.7 5.2 6 12.6 5.7 18.3.8 6-5.3 6.5-13.3 1.6-18.9-5.3-5.9-12.7-5.6-18.4-.6m10.4 10.8c0 .3 0 1.1-.1 1.4 1.5-.1 4.4-.2 4.9-.2 1.8 0 3.2.1 4 .1-.4 2.2-1.5 4.3-3.4 6-4.9 4.3-11 3.1-14.9-1.3-4.7-5.4-4.4-11.7.2-15.7 4.7-4.2 10.5-3.6 14.9 1.4 2.1 2.4 3.1 5 3.3 7.4-1.5.1-7 .2-8.9-.2v1.1M5.9 4.8C.1 9.9-.8 17.7 4.3 23.5c5.2 6 12.6 5.7 18.3.8 6-5.3 6.5-13.3 1.6-18.9C19-.5 11.6-.2 5.9 4.8M21.7 23c-4.9 4.3-11 3.1-14.9-1.3C2.1 16.3 2.4 10 7 6c4.7-4.2 10.5-3.6 14.9 1.4 4.7 5.3 4.2 11.7-.2 15.6M53.5 4.8c-5.8 5.1-6.6 12.8-1.5 18.7 5.2 6 12.6 5.7 18.3.8 6-5.3 6.5-13.3 1.6-18.9-5.3-5.9-12.7-5.6-18.4-.6m10.6 10.8c0 .3 0 1.1-.1 1.4 1.5-.1 4.4-.2 4.9-.2.8 0 2.4.2 3.8.3-.4 2.2-1.5 4.2-3.4 5.8-4.9 4.3-11 3.1-14.9-1.3C49.7 16.3 50 10 54.6 6c4.7-4.2 10.5-3.6 14.9 1.4 2.1 2.4 3.2 5 3.3 7.5-1.6 0-7.2.1-8.8-.3 0 .3.1.8.1 1m163.8-2.3l-3.2-1.2c-2.3-.9-3.9-2-3.9-4.6 0-2.7 1.9-4.1 4.3-4.1 0 0 3.5-.3 4.7 1.8l.4-.1c.2-.7.5-.8.8-1.6-1.7-1.3-3.5-1.7-5.5-1.7-4 0-7.4 2.3-7.4 6.7 0 2.9 1.5 5.2 4.4 6.3l3 1.2c2 .8 3.8 2.3 3.8 4.7 0 3.1-2 5.1-5.1 5.1-2.1 0-4.1-.2-5.8-2.4l-.4.1c0 .7-.2 1.1-.4 1.8 2 1.9 4 2.2 5.8 2.2 5 0 8.6-3.4 8.6-8 .1-3-1.4-5.2-4.1-6.2M191 4.8c-5.8 5.1-6.6 12.8-1.5 18.7 5.2 6 12.6 5.7 18.3.8 6-5.3 6.5-13.3 1.6-18.9-5.2-5.9-12.7-5.6-18.4-.6M206.8 23c-4.9 4.3-11 3.1-14.9-1.3-4.7-5.4-4.4-11.7.2-15.7 4.8-4.2 10.6-3.6 14.9 1.4 4.7 5.3 4.2 11.7-.2 15.6M144.2 4.8c-5.8 5.1-6.6 12.8-1.5 18.7 5.2 6 12.6 5.7 18.3.8 6-5.3 6.5-13.3 1.6-18.9-5.2-5.9-12.6-5.6-18.4-.6M160 23c-4.9 4.3-11 3.1-14.9-1.3-4.7-5.4-4.4-11.7.2-15.7 4.7-4.2 10.6-3.6 14.9 1.4 4.7 5.3 4.2 11.7-.2 15.6M87.9 2.2c-1.1 2.5-8.7 20.3-11.3 24.7.3 0 1-.1 1.4-.1.4 0 .9 0 1.3.1.6-1.8 2.5-7 3.6-9.3 1.4-.2 2.9-.3 4.2-.3 1.2 0 3 0 4.8.3.9 1.9 2.8 7 3.8 9.3.5 0 1.5-.1 2-.1.4 0 1.4 0 1.9.1C97.1 22.1 90 5.2 88.8 2.2h-.9zm-.4 13.3c-1.1 0-2.6-.1-3.4-.2l3.4-8c2 4.5 2.7 6 3.4 8-1.2.1-2.6.2-3.4.2M36.2 2.1l-1.5.1-1.5-.1v24.7h5.7c4.1.1 4.5.2 5.8.5.8-.8 3.2-1.4 3.2-1.4l.1-.7H36.2V2.1zm138.7 0l-1.5.1-1.5-.1v24.7h5.7c4.1.1 4.5.2 5.8.5.8-.8 3.2-1.4 3.2-1.4l.1-.7H175V2.1z" />
                        </svg>
                        {isAdmin ? (<div>Hello {isAdmin}</div>) : null}
                    </div>
                    <div className={css.linksWrapper}>
                        <div className={css.links}>
                            <div className={`${css.link} ${active === 'news' ? css.active : ''}`}
                                 onClick={() => this.setState({
                                     active: 'news'
                                 })}>News
                            </div>
                            <a href={this.portfolio} target='_blank' className={css.link}>Portfolio</a>
                            <a href={this.resume} target='_blank' className={css.link}>CV</a>
                            <div className={`${css.link} ${active === 'contact' ? css.active : ''}`}
                                 onClick={() => this.setState({
                                     active: 'contact'
                                 })}>Contact
                            </div>
                        </div>
                        {isAdmin ? (
                            <div className={css.links}>
                                <UploadField
                                    onFiles={this.addNew}
                                    containerProps={{
                                        className: css.link
                                    }}
                                    uploadProps={{
                                        accept: '.pdf',
                                    }}>
                                    <div>Add New</div>
                                </UploadField>
                                <UploadField
                                    onFiles={uploadPortfolio}
                                    containerProps={{
                                        className: css.link
                                    }}
                                    uploadProps={{
                                        accept: '.pdf',
                                    }}>
                                    <div>Change Portfolio</div>
                                </UploadField>
                                <UploadField
                                    onFiles={uploadCV}
                                    containerProps={{
                                        className: css.link
                                    }}
                                    uploadProps={{
                                        accept: '.pdf',
                                    }}>
                                    <div>Change CV</div>
                                </UploadField>
                                <UploadField
                                    onFiles={this.uploadImage}
                                    containerProps={{
                                        className: css.link
                                    }}
                                    uploadProps={{
                                        accept: '.jpg, .png',
                                    }}>
                                    <div>{`Image: ${active}`}</div>
                                </UploadField>
                            </div>
                        ) : null}
                    </div>
                </header>
                {
                    active === 'news' ?
                        (<div className={css.body}>
                            {exhibitions.map((exhibition, ind, all) => (
                                <Box>
                                    <NewsItem
                                        isLast={ind === all.length - 1}
                                        isFirst={ind === 0}
                                        isAdmin={Boolean(isAdmin)}
                                        name={exhibition.name}
                                        exhibitionType={exhibition.exhibitionType}
                                        address={exhibition.address}
                                        timeAndPlace={exhibition.timeAndPlace}
                                        features={exhibition.features}
                                        id={exhibition.id}
                                        onChange={this.onChangeExhibition}
                                        save={this.onSaveExhibition}
                                        remove={this.onRemove}
                                        moveUp={this.onMoveUp}
                                        moveDown={this.onMoveDown}
                                        invitation={getUrl(`api/exhibitions/invitation/${exhibition.id}`)}
                                        onChangeInvitation={uploadInvitation}
                                    />
                                </Box>
                            ))}
                        </div>) :
                        (<div className={css.bodyContact}>
                            <Box>
                                <Contact isAdmin={Boolean(isAdmin)} />
                            </Box>
                        </div>)
                }
            </div>
        );
    }
}
