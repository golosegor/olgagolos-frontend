declare const require: {
    <T>(path: string): T;
    (paths: string[], callback: (...modules: any[]) => void): any;
};
