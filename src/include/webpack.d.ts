declare var module: {
    hot: {
        accept: (name: string, clbk: () => void) => void;
    }
};
